﻿using System.Collections.Generic;

namespace Reto.Mudanza.Tech.Business.Interfaces
{
    public interface IProcessFileManager
    {
        string ProcessFile(List<int> listOfValues);
    }
}