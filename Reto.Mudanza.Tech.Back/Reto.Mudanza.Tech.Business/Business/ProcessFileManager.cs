﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Reflection.Metadata.Ecma335;
using System.Text;
using Reto.Mudanza.Tech.Business.Interfaces;
using Reto.Mudanza.Tech.Models;
using Reto.Mudanza.Tech.Utilities.Utils;

namespace Reto.Mudanza.Tech.Business
{
    public class ProcessFileManager : IProcessFileManager
    {
        private readonly IGeneralConfiguration _generalConfiguration;

        public ProcessFileManager(IGeneralConfiguration generalConfiguration)
        {
            _generalConfiguration = generalConfiguration;
        }

        /// <summary>
        ///Method that processes the values ​​obtained from the file.
        /// Valid if the total number of working days is between 1 and 500
        /// </summary>
        /// <param name="listOfValues">List of values ​​to process obtained from the file</param>
        /// <returns>Returns a string with the expected format, indicating the number of trips to be made for each day
        /// </returns>
        public string ProcessFile(List<int> listOfValues)
        {
            var result = string.Empty;
            try
            {
                if (listOfValues == null || listOfValues.Count <= 0) return result;
                if (!(1 <= listOfValues[0] && listOfValues[0] <= _generalConfiguration.TotalDaysToWork)) throw new Exception($"{_generalConfiguration.InvalidDaysWorks}");

                var calculateTotalTravel = CalculateTotalTravel(listOfValues);
                if (calculateTotalTravel?.Count > 0)
                    result = OutputString(calculateTotalTravel);
                return result;
            }
            catch (Exception e)
            {
                throw;
            }

        }

        /// <summary>
        /// Method that obtains the quantity of items to be transported for each day
        /// </summary>
        /// <param name="listValues">Receive the list of values</param>
        /// <returns>Returns the number of trips to be made for each day</returns>
        private  List<int> CalculateTotalTravel(IList<int> listValues)
        {
            try
            {
                var workDays = 0;
                workDays = listValues[0];
                listValues.RemoveAt(0);
                var totalTrips = 1;
                var numberOfTrips = 0;
                var totalLoads = new List<LazyLoad>();
                for (var i = 0; i < workDays; i++)
                {
                    var lst = new List<int>();
                    var cantTravel = listValues[numberOfTrips];
                    for (var j = 1; j <= cantTravel; j++)
                    {
                        lst.Add(listValues[totalTrips + i]);
                        totalTrips += 1;
                    }
                    var load = new LazyLoad
                    {
                        AmountOfTrips = cantTravel,
                        AmountOfWeight = lst.OrderBy(x => x).Reverse().ToList()
                    };
                    totalLoads.Add(load);
                    numberOfTrips += cantTravel + 1;
                }
                var totalFinal = GetAmountTravel(totalLoads);
                return totalFinal;
            }
            catch (Exception e)
            {
                throw;
            }
        }

        /// <summary>
        /// Method that obtains number of trips to be made
        /// It is validated if the number of elements to transport is between 1 and 100
        /// </summary>
        /// <param name="totalLoads"></param>
        /// <returns>Returns the total trips</returns>
        private  List<int> GetAmountTravel(IEnumerable<LazyLoad> totalLoads)
        {
            var totalTrips = new List<int>();
            try
            {
                foreach (var load in totalLoads)
                {
                    if (!(1 <= load.AmountOfTrips) && (load.AmountOfTrips <= _generalConfiguration.TotalElements)) throw new Exception($"{_generalConfiguration.InvalidTotalElements}");
                    var tripsMade = PossibleTrips(load.AmountOfWeight);
                    totalTrips.Add(tripsMade);
                }
                return totalTrips;
            }
            catch (Exception e)
            {
                throw;
            }
        }

        /// <summary>
        /// Method that calculates the number of trips for each day.
        /// It is validated if the weight to be transported is between 1 and 100
        /// </summary>
        /// <param name="amountOfWeight"></param>
        /// <returns>Maximum number of trips to be made for day</returns>
        private int PossibleTrips(List<int> amountOfWeight)
        { 
            var totalT = 0;
            try
            {
                var element = 1;
                var numberTrips = amountOfWeight.Count;
                foreach (var weight in amountOfWeight)
                {
                    //Regla 1 ≤ Wi ≤ 100
                    if (1 > weight || (weight > _generalConfiguration.TotalWeigthWork)) throw new Exception($"ProcessFileManager --> PossibleTrips --> {_generalConfiguration.InvalidTotalElements}"); 
                    if (weight >= 50)
                    {
                        totalT++;
                        numberTrips--;
                    }
                    else
                    {
                        if (element > numberTrips) continue;
                        while (element <= numberTrips)
                        {
                            if ((weight * element) >= 50)
                            {
                                totalT++;
                                break;
                            }

                            element++;
                        }

                        numberTrips -= element;
                    }
                }
                return totalT;
            }
            catch (Exception e)
            {
                throw;
            }

        }

        /// <summary>
        /// Method that outputs the string in the indicated format
        /// </summary>
        /// <param name="lstResult">List of values ​​to write to the output file</param>
        /// <returns>String with the indicated format</returns>
        private  string OutputString(List<int> lstResult)
        {
            var count = 1;
            var finalString = new StringBuilder();
            for (var i = lstResult.Count - 1; i >= 0; i--)
            {
                finalString.AppendLine($"{_generalConfiguration.CaseNum} {count}: {lstResult[i]}");
                count++;
            }
            return finalString.ToString();
        }
    }
}
