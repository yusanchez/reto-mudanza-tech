﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Net.Mime;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Reto.Mudanza.Tech.Models;
using Reto.Mudanza.Tech.Utilities.Utils;
using System.Web.Http.Cors;
using Reto.Mudanza.Tech.Business.Interfaces;

namespace Reto.Mudanza.Tech.Api.Controllers
{

    /// <summary>
    /// 
    /// </summary>
    [Route("api/lazyLoading")]
    [ApiController]
    public class LazyLoadingController : ControllerBase
    {
        private readonly IFileManager _fileManager;
        private readonly IProcessFileManager _processFileManager;
        private readonly IGeneralConfiguration _generalConfiguration;

        private readonly IWebHostEnvironment _hostingEnvironment;


        /// <summary>
        /// Controller LazyLoading
        /// </summary>
        /// <param name="hostingEnvironment"></param>
        /// <param name="fileManager"></param>
        /// <param name="processFileManager"></param>
        /// <param name="generalConfiguration"></param>
        public LazyLoadingController(IWebHostEnvironment hostingEnvironment, IFileManager fileManager, 
            IProcessFileManager processFileManager, IGeneralConfiguration generalConfiguration)
        {
            _hostingEnvironment = hostingEnvironment;
            _fileManager = fileManager;
            _processFileManager = processFileManager;
            _generalConfiguration = generalConfiguration;
        }

        /// <summary>
        /// Method to process file
        /// </summary>
        /// <param name="file">File name</param>
        /// <returns>Status</returns>
        [HttpPost, DisableRequestSizeLimit]
        [ProducesResponseType(StatusCodes.Status200OK)]
        [ProducesResponseType(StatusCodes.Status400BadRequest)]
        [ProducesResponseType(StatusCodes.Status404NotFound)]
        public IActionResult Post(IFormFile file)
        {
            try
            {
                var fileInformation = new FileInformation();
                if (file == null || file.Length == 0) return NotFound(new { status = false, text = "File content is empty" });

                fileInformation.FileName = ContentDispositionHeaderValue.Parse(file.ContentDisposition).FileName.Trim('"');
                fileInformation.UploadFolderName = _generalConfiguration.UploadFolder;
                fileInformation.PathFileName = fileInformation.UploadFolderName;
                fileInformation.TargetPath = Path.Combine(fileInformation.PathFileName, fileInformation.FileName);
                fileInformation.File = file;

                var listOfValues = _fileManager.ReadFile(fileInformation);
                fileInformation.FileContent = _processFileManager.ProcessFile(listOfValues);
                fileInformation.TargetOutputPath = Path.Combine(fileInformation.UploadFolderName, _generalConfiguration.FileNameOutput);

                _fileManager.WriteFile(fileInformation);

                return Ok(new { status = true, text = "Process success" });
            }
            catch (Exception e)
            {
                Log.Log.Logger($"Error processing file = {e.Message}");
                return BadRequest($"Error while processing file {e.Message}");
            }
        }
        /// <summary>
        /// Download file
        /// </summary>
        /// <returns>file downloaded</returns>
        [HttpGet]
        [Route("Download")]
        public FileStream Download()
        {
            try
            {
                var router = $"{_generalConfiguration.UploadFolder}{_generalConfiguration.FileNameOutput}";
                return new FileStream(router, FileMode.Open, FileAccess.Read);
            }
            catch (Exception e)
            {
                Log.Log.Logger($"Error download file = {e.Message}");
                return null;
            }
           
        }

    }
}