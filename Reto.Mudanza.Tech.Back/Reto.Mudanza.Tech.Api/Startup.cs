using System;
using System.IO;
using System.Reflection;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;
using Microsoft.Extensions.Options;
using Microsoft.OpenApi.Models;
using Reto.Mudanza.Tech.Business;
using Reto.Mudanza.Tech.Business.Interfaces;
using Reto.Mudanza.Tech.Utilities.Utils;

namespace Reto.Mudanza.Tech.Api
{
    public class Startup
    {
        public IConfiguration Configuration { get; }
        public static readonly string MyAllowSpecificOrigins = "CorsPolicy";
        private SwaggerConfiguration _swaggerConfiguration;

        public Startup(IConfiguration configuration, IWebHostEnvironment env)
        {
            Configuration = configuration;
            var builder = new ConfigurationBuilder()
                .SetBasePath(env.ContentRootPath)
                .AddJsonFile("appsettings.json", optional: false, reloadOnChange: true)
                .AddEnvironmentVariables();
            Configuration = builder.Build();
        }


        // This method gets called by the runtime. Use this method to add services to the container.
        public void ConfigureServices(IServiceCollection services)
        {
            _swaggerConfiguration = Configuration.GetSection(nameof(SwaggerConfiguration)).Get<SwaggerConfiguration>();
            services.AddControllers();

            services.Configure<GeneralInformation>(
                Configuration.GetSection(nameof(GeneralInformation)));

            services.AddSingleton<IGeneralConfiguration>(sp =>
                sp.GetRequiredService<IOptions<GeneralInformation>>().Value);
            
            services.AddTransient<IFileManager, FileManager>();
            services.AddTransient<IProcessFileManager, ProcessFileManager>();

            services.AddSwaggerGen(c =>
            {
                c.SwaggerDoc(_swaggerConfiguration.DocName, new OpenApiInfo
                {
                    Version = _swaggerConfiguration.DocInfoVersion,
                    Title = _swaggerConfiguration.DocInfoTitle,
                    Description = _swaggerConfiguration.DocInfoDescription,
                });

                var xmlFile = $"{Assembly.GetExecutingAssembly().GetName().Name}.xml";
                var xmlPath = Path.Combine(AppContext.BaseDirectory, xmlFile);
                c.IncludeXmlComments(xmlPath);
            });
            services.AddCors(options =>
            {
                options.AddPolicy(MyAllowSpecificOrigins,
                    builder =>
                    {
                        builder.AllowAnyOrigin()
                            .AllowAnyMethod()
                            .WithMethods("GET")
                            .WithMethods("POST")
                            .WithMethods("PUT")
                            .WithMethods("DELETE")
                            .AllowAnyOrigin()
                            .SetIsOriginAllowed((host) => true)
                            .AllowAnyHeader();
                    });
            });

        }

        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure(IApplicationBuilder app, IWebHostEnvironment env)
        {
            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
            }

            app.UseRouting();

            app.UseAuthorization();
            app.UseCors(MyAllowSpecificOrigins);
            app.UseEndpoints(endpoints =>
            {
                endpoints.MapControllers();
            });

            app.UseSwagger();

            app.UseSwaggerUI(c =>
            {
                var swaggerJsonBasePath = string.IsNullOrWhiteSpace(c.RoutePrefix) ? "." : "..";
                c.SwaggerEndpoint($"{swaggerJsonBasePath}{_swaggerConfiguration.EndpointUrl}", _swaggerConfiguration.EndpointDescription);
            });
        }
    }
}
