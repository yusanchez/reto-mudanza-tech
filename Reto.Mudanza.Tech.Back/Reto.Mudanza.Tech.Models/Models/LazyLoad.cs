﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Reto.Mudanza.Tech.Models
{
    public class LazyLoad
    {
        public int AmountOfTrips { get; set; }
        public List<int> AmountOfWeight { get; set; }
    }
}