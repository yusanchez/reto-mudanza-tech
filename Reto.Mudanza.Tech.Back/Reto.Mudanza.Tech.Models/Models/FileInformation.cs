﻿using Microsoft.AspNetCore.Http;
using System;
using System.Collections.Generic;
using System.Text;

namespace Reto.Mudanza.Tech.Models
{
  
    public class FileInformation
    {

        public string FileName { get; set; }
        public string PathFileName { get; set; }
        public string UploadFolderName { get; set; }
        public string TargetPath { get; set; }
        public string FileContent { get; set; }
        public string TargetOutputPath { get; set; }

        public IFormFile File { get; set; }

    }
}