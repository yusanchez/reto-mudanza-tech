﻿using System;
using System.Diagnostics;
using System.Globalization;
using System.IO;
using System.Windows;

namespace Reto.Mudanza.Tech.Log
{
    public class Log
    {
        public static void VerifyDir(string path)
        {
            try
            {
                var dir = new DirectoryInfo(path);
                if (!dir.Exists)
                    dir.Create();
                
            }
            catch
            {
                // ignored
            }
        }

        public static void Logger(string lines)
        {
            //var path = System.IO.Path.GetDirectoryName(
            //    System.Reflection.Assembly.GetExecutingAssembly().GetName().CodeBase);
            var path = @"D:\Personal";
            VerifyDir(path);
            var fileName = DateTime.Now.Day.ToString() + DateTime.Now.Month.ToString() + DateTime.Now.Year.ToString() + "_Logs.txt";
            try
            {
                var file = new System.IO.StreamWriter(path + fileName, true);
                file.WriteLine(DateTime.Now.ToString(CultureInfo.InvariantCulture) + ": " + lines);
                file.Close();
            }
            catch (Exception)
            {
                // ignored
            }
        }
    }
}