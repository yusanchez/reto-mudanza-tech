﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Reto.Mudanza.Tech.Utilities.Utils
{
    public class SwaggerConfiguration
    {
        public string EndpointDescription { get; set; }

        public string EndpointUrl { get; set; }

        public string DocName { get; set; }

        public string DocInfoTitle { get; set; }

        public string DocInfoVersion { get; set; }

        public string DocInfoDescription { get; set; }
    }
}
