﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Reto.Mudanza.Tech.Utilities.Utils
{
    public class GeneralInformation : IGeneralConfiguration
    {

        public string InvalidDaysWorks { get; set; }
        public string InvalidTotalElements { get; set; }
        public string CaseNum { get; set; }
        public string FileNameOutput { get; set; }
        public string UploadFolder { get; set; }
        public string FileNotExists { get; set; }
        public int TotalElements { get; set; }
        public int TotalDaysToWork { get; set; }
        public int TotalWeigthWork { get; set; }
    }

    public interface IGeneralConfiguration
    {
        string InvalidDaysWorks { get; set; }
        string InvalidTotalElements { get; set; }
        string CaseNum { get; set; }
        string FileNameOutput { get; set; }
        string UploadFolder { get; set; }
        string FileNotExists { get; set; }
        int TotalElements { get; set; }
        int TotalDaysToWork { get; set; }
        int TotalWeigthWork { get; set; }

    }
}
