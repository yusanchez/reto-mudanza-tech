﻿using System;

namespace Reto.Mudanza.Tech.Utilities.Utils.Exceptions
{
    /// <summary>
    /// Clase para el manejo de excepciones a nivel del archivo
    /// </summary>
    public class FileException : Exception
    {
        private string codMessage { get; set; }
        public FileException()
            : base()
        { }

        public FileException(string message)
            : base(message)
        {
            this.codMessage = codMessage;

        }
        public FileException(string message, Exception innerException)
            : base(message, innerException)
        {
            this.codMessage = codMessage;

        }
    }
}