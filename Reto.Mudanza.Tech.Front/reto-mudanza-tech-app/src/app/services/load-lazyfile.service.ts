import { Injectable } from '@angular/core';
import { HttpHeaders, HttpClient } from '@angular/common/http';

@Injectable({
  providedIn: 'root'
})
export class LoadLazyfileService {
  url= "http://localhost:53344";
  constructor(private httpClient: HttpClient) { }

  GuardarArchivo(file: File) {

    const input = new FormData();
    input.append("file", file);


    const headers = new HttpHeaders(
      {
          'Content-Type': 'multipart/form-data'
      })

    return this.httpClient.post(`${this.url}/api/lazyLoading`, input, {observe: 'response'});
  }
  Descargar(){
    return this.httpClient.get(`${this.url}/api/lazyLoading/Download`, { responseType: 'blob' });
  }
}
