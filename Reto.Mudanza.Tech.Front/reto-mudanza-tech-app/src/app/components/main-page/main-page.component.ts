import { LoadLazyfileService } from './../../services/load-lazyfile.service';
import { Component, OnInit } from '@angular/core';
import { FormGroup, FormBuilder, Validators, FormControl } from '@angular/forms';
import Swal from 'sweetalert2';

@Component({
  selector: 'app-main-page',
  templateUrl: './main-page.component.html',
  styleUrls: ['./main-page.component.css']
})
export class MainPageComponent implements OnInit {

  forma:FormGroup;
  selectedFile: File = null;

  constructor(private fb:FormBuilder,private services:LoadLazyfileService) { }


  ngOnInit(): void {
    this.crearFormulario();
  }
  get cedulaNovalido(){
    return this.forma.get('cedula').invalid && this.forma.get('cedula').touched
   }
   get validarArchivo(){
    return this.forma.get('file').invalid && this.forma.get('file').touched
   }

  crearFormulario(){

    this.forma=this.fb.group({
      cedula  :['', [Validators.required, Validators.minLength(10) ]  ],
      file: new FormControl(null),
      CargarArchivo: new FormControl(null)
    });

  }


  CargarArchivo(event) {
    this.selectedFile = <File>event.target.files[0];
    console.log("Errore evento input", this.selectedFile)

  }

  GuardarArchivo() {

    if(this.forma.invalid)return false;
    this.services.GuardarArchivo(this.selectedFile).subscribe(response => {
      console.log('lo que tiene el response', response);
      if(response.status == 200){
        Swal.fire({
          title: 'Do you want to save the changes?',
          showCancelButton: true,
          confirmButtonText: `Descargar Archivo`
        }).then((result) => {
          /* Read more about isConfirmed, isDenied below */
          if (result.isConfirmed) {
            this.Descargar();
          }
        })
      }
      else{
        Swal.fire({
          icon: 'error',
          title: 'Oops...',
          text: 'Something went wrong!',
          footer: '<a href>Why do I have this issue?</a>'
        })
      }

    });
  }
  Descargar() {
    console.log("Entramos a descargar")
    this.services.Descargar().subscribe((data) => {

      var downloadURL = window.URL.createObjectURL(data);
      var link = document.createElement('a');
      link.href = downloadURL;
      link.download = "lazy_loading_example_output.txt";
      link.click();

    });
  }
}
